# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include mysql_workbench::install
class mysql_workbench::install {
  package { $mysql_workbench::package_name:
    ensure => $mysql_workbench::package_ensure,
  }
}
