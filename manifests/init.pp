# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include mysql_workbench
class mysql_workbench (
  String        $package_ensure,
  Array[String] $package_name,
) {
  contain 'mysql_workbench::install'
}
